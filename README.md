# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000

Terraform command for docker-compose:
# Terraform format
```
docker-compose -f deploy/docker-compose.yml run --rm terraform fmt
```
# Terraform validate
```
docker-compose -f deploy/docker-compose.yml run --rm terraform validate
```
# Terraform plan
```
docker-compose -f deploy/docker-compose.yml run --rm terraform plan -out tfplan
```
# Terraform apply
```
docker-compose -f deploy/docker-compose.yml run --rm terraform apply "tfplan" 
```
# Terraform destroy
```
docker-compose -f deploy/docker-compose.yml run --rm terraform plan -destroy -out=tfplan
```

# Section 72. Add Security Group
Inbound access -> port 22 (SSH)
Outbound access -> port 443 (HTTPS) & 80 (HTTP)
Outbound access -> port 5432 (Postgres)

# 79. Create container definition template:
Reference:
https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#container_definitions

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition#example-usage

https://gitlab.com/LondonAppDev/recipe-app-api-devops-course-material/snippets/1945267

80. 
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition

# 82. UPDATE: Create and deploy service
Previously the deployment process was broken when AWS updated the default version of AWS Fargate from 1.3.0 to 1.4.0.

We fixed this earlier in the course (see IMPORTANT UPDATE: Update Dockerfile to run entrypoint lecture), however it’s a good idea to pin the version of AWS Fargate to prevent this issue from happening again.

To do this, modify the aws_ecs_service.api resource that we created in the previous lesson to contain the following line:

platform_version = "1.4.0" 

(See full change in this commit: https://gitlab.com/LondonAppDev/recipe-app-api-devops/-/commit/416c01e487cadbf05080d678992403a0910f4b73#e0ca724c1c3b583b098b6de4b61a4e679307386c_115_115)

This means developers can decide when they wish to use later versions and perform testing before pushing to production.

For real-world deployments, it’s a good idea for project maintainers to subscribe to AWS email notifications to stay updated on upcoming changes so you can plan accordingly.

docker run -it -e DB_HOST=raad-staging-db.cc4wpftlybit.us-east-1.rds.amazonaws.com -e DB_NAME=recipe -e DB_USER=recipeapp -e DB_PASS=“changeme” 692222188816.dkr.ecr.us-east-1.amazonaws.com/receipe-rjfapp-api-devops:latest sh -c "python manage.py wait_for_db && python manage.py createsuperuser"