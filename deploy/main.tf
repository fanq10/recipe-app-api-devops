terraform {
  backend "s3" {
    bucket         = "receipe-rjfapp-api-devops-tfstate"
    key            = "receipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = "true"
    dynamodb_table = "receipe-rjfapp-api-devops-tfstate-lock"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.21.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Enviornment = terraform.workspace
    Porject     = var.projects
    Owner       = var.contact
    ManagedBy   = "terraform"
  }
}

data "aws_region" "current" {}
