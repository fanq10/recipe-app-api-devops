output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  value = aws_lb.api.dns_name
}

output "initial-aws-setup" {
  value = <<CONTENT

1. aws-vault add rjftrust-terraform

    Enter Access Key ID: AKIA2CK5ILUIKMXFU4BW
    Enter Secret Access Key:
    Added credentials to profile "rjftrust-terraform" in vault

2. aws-vault list

    Profile                  Credentials              Sessions
    =======                  ===========              ========
    default                  -                        -
    rjftrust-terraform       rjftrust-terraform       -


3. aws-vault exec rjftrust-terraform --duration=12h


Terraform Cheat Sheet:
  Terraform format
    docker-compose -f deploy/docker-compose.yml run --rm terraform fmt

  Terraform validate
    docker-compose -f deploy/docker-compose.yml run --rm terraform validate

  Terraform plan
    docker-compose -f deploy/docker-compose.yml run --rm terraform plan -out tfplan

  Terraform apply
    docker-compose -f deploy/docker-compose.yml run --rm terraform apply "tfplan" 

  Terraform destroy
    docker-compose -f deploy/docker-compose.yml run --rm terraform plan -destroy -out=tfplan

  CONTENT
}