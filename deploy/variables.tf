variable "prefix" {
  default = "raad"
}

variable "projects" {
  default = "receipe-app-api-devops"
}

variable "contact" {
  default = "RJF TRUST"
}

variable "db_username" {
  description = "Username for the RDS postgress instance"
}

variable "db_password" {
  description = "Password for the RDS postgress instance"
}

variable "bastion_key_name" {
  default = "receipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "692222188816.dkr.ecr.us-east-1.amazonaws.com/receipe-rjfapp-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "692222188816.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}